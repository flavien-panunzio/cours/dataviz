$(document).ready(function() {
	var nbMots;
	var rayon;
	var inputUser = Array();
	var mesCouleurs = ['#f6e58d','#ffbe76','#ff7979','#badc58','#dff9fb','#f9ca24','#f0932b','#eb4d4b','#6ab04c','#7ed6df','#e056fd'];

	$('.sendtext').click(function() {
		inputUser = $('textarea').val().split(' ');
		resultFunction=compteur(inputUser);
		nbMots=resultFunction.length;
		for (var i = 0; i < nbMots; i++) {
			x=getRandom(10,90);
			y=getRandom(10,40);
			color=getRandomInt(0,mesCouleurs.length);
			split=resultFunction[i].split('_');
			rayon=split[1]*($("svg").height()/2);
			color=mesCouleurs[color];
			mot=split[0];

			a = $('<a class="circle" data-mot="'+mot+'"></a>').appendTo($("svg"));


			svg=$(document.createElementNS('http://www.w3.org/2000/svg','circle')).attr('cx', x+'vw').attr('cy', y+'vh').attr('r', rayon).attr('fill', color);
			$(svg).appendTo(a);

		}
	});
	$(".circle").hover(function(event) {
		mot=$(this).data('mot');
		$('#mot').css({
			'left': event.pageX+'px',
			'top': event.pageY+'px'
		}).text(mot);

	});
});

function compteur(value){
	var compar;
	var increment;
	var result = Array();
	var count = Array();
	var nbMots = value.length;
	for (var i = 0; i < nbMots; i++) {
		increment = 0;
		compar=value[i];
		for (var j = 0; j < nbMots; j++) {
			if (compar==value[j])
				increment++;
		}
		pourcentage=increment/nbMots;
		count[compar]=pourcentage;
	}
	var longTab = 0;
	for (var k in count){
		if (typeof count[k] !== 'function') {
			result[longTab]=k+"_"+count[k];
		}
		longTab++;
	}
	return result;
}

function getRandom(min, max) {
  return Math.random() * (max - min) + min;
}

function getRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min;
}